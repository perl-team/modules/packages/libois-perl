libois-perl (0.10-2) unstable; urgency=medium

  [ Debian Janitor ]
  * Bump debhelper from old 10 to 12.
  * Set debhelper-compat version in Build-Depends.

  [ gregor herrmann ]
  * debian/control: update Build-Depends for cross builds.
  * debian/watch: use uscan version 4.
  * Update 'DEB_BUILD_MAINT_OPTIONS = hardening=+bindnow' to '=+all'.

  [ Debian Janitor ]
  * Bump debhelper from old 12 to 13.
  * Update standards version to 4.1.5, no changes needed.

  [ gregor herrmann ]
  * Update years of packaging copyright.
  * Declare compliance with Debian Policy 4.6.1.
  * Set Rules-Requires-Root: no.

 -- gregor herrmann <gregoa@debian.org>  Sun, 14 Aug 2022 20:39:06 +0200

libois-perl (0.10-1) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ Florian Schlichting ]
  * Import Upstream version 0.10
  * Drop fix-ois-header.patch, use-config-ccflags.patch: applied upstream

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ Axel Beckert ]
  * Replace search.cpan.org URL in long description with metacpan.org URL.
  * Remove "Enhances: libogre-perl" as the package planned to be removed.
  * Declare compliance with Debian Policy 3.9.6 (no other changes needed).

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * Update years of packaging copyright.
  * Mark package as autopkgtest-able.
  * Declare compliance with Debian Policy 4.1.4.
  * Bump debhelper compatibility level to 10.
  * Set bindnow linker flag in debian/rules.

 -- gregor herrmann <gregoa@debian.org>  Sun, 08 Apr 2018 18:58:44 +0200

libois-perl (0.05-3) unstable; urgency=low

  * Team upload
  * Add fix-ois-header.patch to fix a FTBFS (Closes: #669448)
  * Bump debhelper compat level to 9
  * Update d/copyright to copyright-format 1.0
  * Bump Standards-Version to 3.9.3

 -- Alessandro Ghedini <ghedo@debian.org>  Thu, 19 Apr 2012 21:46:12 +0200

libois-perl (0.05-2) unstable; urgency=low

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ Salvatore Bonaccorso ]
  * debian/copyright: Replace DEP5 Format-Specification URL from
    svn.debian.org to anonscm.debian.org URL.

  [ gregor herrmann ]
  * Add a patch to include $Config{ccflags} in CCFLAGS.
    (Closes: #636655)
  * Switch to source format 3.0 (quilt).
  * debian/copyright: update formatting.
  * Bump debhelper compatibility level to 8.
  * Set Standards-Version to 3.9.2 (no changes).

 -- gregor herrmann <gregoa@debian.org>  Mon, 08 Aug 2011 21:09:29 +0200

libois-perl (0.05-1) unstable; urgency=low

  * Initial release (closes: #556197).

 -- gregor herrmann <gregoa@debian.org>  Sun, 15 Nov 2009 21:02:41 +0100
